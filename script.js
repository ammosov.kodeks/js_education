const user = {
  
  name: "Дмитрий",
  birth: "03.12",
  pic: "https://cdn1.iconfinder.com/data/icons/user-pictures/100/male3-256.png",
  userAgent: navigator.userAgent,
  skills: ['пение', 'рисование', 'игра на гитаре']
}

let wrapper = document.createElement('div');

wrapper.style.border = '2px solid red';
wrapper.style.margin = '0 auto';
wrapper.style.padding = '16px';
wrapper.style.textAlign = 'center';
wrapper.style.maxWidth = '50%';

let ul = document.createElement('ul');

for (item in user) {
  let li = document.createElement('li');
  
  if (item == 'pic') {
    let img = document.createElement('img');
    img.src = user[item];
    li.append(img);
    ul.prepend(li);
  }
  
  else if(Array.isArray(user[item])) {
    let list = document.createElement('ul');
    for (i in user[item]) {
      let listItem = document.createElement('li');
      listItem.innerHTML = user[item][i];
      list.append(listItem);
    }
    ul.append(list);
  }

  else {
    li.innerHTML = user[item];
    ul.append(li);
  } 
}

document.body.append(wrapper);
wrapper.append(ul);

let coords = document.createElement('div');
document.body.append(coords);

document.body.onmousemove = function(event) {
  coords.innerHTML = `Координаты мыши: x: ${event.offsetX}; y: ${event.offsetY}`;
}

metrics = [
  wrapper.offsetWidth,
  wrapper.clientWidth, 
  ul.clientWidth,
  wrapper.offsetTop,
  wrapper.offsetLeft,
  document.body.offsetWidth - wrapper.offsetWidth - wrapper.offsetLeft
]

let metricsList = document.createElement('ul');
  for (i in metrics) {
    let listItem = document.createElement('li');
    listItem.innerHTML = metrics[i];
    metricsList.append(listItem);
  }
   
wrapper.append(metricsList);




  





